package com.hendisantika.springmvcrudsample.controller;

import com.hendisantika.springmvcrudsample.dao.EmployeeDao;
import com.hendisantika.springmvcrudsample.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : SpringMVC-CRUD-Sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 31/12/19
 * Time: 06.32
 */
@Controller
@RequestMapping("/employee")
public class EmployeeController {
    @Autowired
    private EmployeeDao employeeDao;

    /*It displays a form to input data, here "command" is a reserved request attribute
     *which is used to display object data into form
     */
    @RequestMapping("/form")
    public String showFormEmployee(Model m) {
        m.addAttribute("command", new Employee());
        return "form";
    }

    /*It saves object into database. The @ModelAttribute puts request data
     *  into model object. You need to mention RequestMethod.POST method
     *  because default request is GET*/
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveEmployee(@ModelAttribute("emp") Employee employee) {
        employeeDao.save(employee);
        return "redirect:/view";//will redirect to viewemp request mapping
    }

    /* It provides list of employees in model object */
    @RequestMapping("/view")
    public String viewEmployee(Model m) {
        List<Employee> list = employeeDao.getEmployees();
        m.addAttribute("list", list);
        return "view";
    }

    /* It displays object data into form for the given id.
     * The @PathVariable puts URL data into variable.*/
    @RequestMapping(value = "/edit/{id}")
    public String editEmployee(@PathVariable int id, Model m) {
        Employee emp = employeeDao.getEmpById(id);
        m.addAttribute("command", emp);
        return "edit";
    }

    /* It updates model object. */
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String editSaveEmployee(@ModelAttribute("emp") Employee employee) {
        employeeDao.update(employee);
        return "redirect:/view";
    }

    /* It deletes record for the given id in URL and redirects to /viewemp */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String deleteEmployee(@PathVariable int id) {
        employeeDao.delete(id);
        return "redirect:/view";
    }
}
