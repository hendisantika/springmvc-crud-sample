package com.hendisantika.springmvcrudsample.entity;

import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * Project : SpringMVC-CRUD-Sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 31/12/19
 * Time: 06.26
 */

@Data
public class Employee {
    private int id;
    private String name;
    private float salary;
    private String designation;
}
